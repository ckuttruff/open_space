# Open Space PDX

## About

We are a Portland organization dedicated to providing a safe, accessible, stimulating and environmentally-respectful project space for photographers and other hobbyists.  We hope to offer classes, technical talks and quality tools / workspaces to help individuals pursue their goals.

## License

Copyright © 2013 Christopher Kuttruff

Distributed under the MIT License (see LICENSE for details)
