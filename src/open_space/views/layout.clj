(ns open-space.views.layout
  (:use [hiccup.core])
  (:require [open-space.views.helpers :as h]))

(def nav {:home    {:name "Home" :href "#"}
          :about   {:name "About" :href "#about"}
          :contact {:name "Contact" :href "#contact"}})

(defn main [body]
  (html
   [:html
    [:head
     ;; Meta info and title
     [:meta {:charset "utf-8"}]
     [:meta {:name "viewport" :content "width=device-width, initial-scale=1.0"}]
     [:meta {:name "author" :content "Christopher Kuttruff"}]
     [:title "Open Space PDX | Darkroom / Workspace / Café"]
     [:link {:rel "shortcut icon" :href "/img/favicon.ico"}]

     (h/include-css "/css/bootstrap.min.css")
     (h/include-css "/css/custom.css")]

    [:body
     [:div.navbar.navbar-inverse.navbar-fixed-top
      [:div.container
       [:div.navbar-header
        [:button.navbar-toggle {:type "button"
                                :data-toggle "collapse"
                                :data-target ".navbar-collapse"}
         [:span.sr-only "Toggle Navigation"]
         (repeat 3 [:span.icon-bar])]
        [:a.navbar-brand {:href "#"} "Open Space PDX"]]
       [:div.navbar-collapse.collapse
        [:ul.nav.navbar-nav
         [:li.active [:a {:href "/"} "Home"]]
         [:li [:a {:href "/darkroom"}  "Darkroom"]]
         [:li [:a {:href "/workspace"} "Workspace"]]
         [:li [:a {:href "/cafe"}      "Café"]]
         [:li [:a {:href "/about"}     "About"]]
         [:li [:a {:href "/contact"}   "Contact"]]
         ;; [:li [:a {:href "/photography"} "Gallery"]]
         ]]]]
     [:div.container
      [:div.row
       [:div.col-lg-3 [:div.container]]
       [:div.col-lg-9 [:div.container.main body]]]]

     ;; Load js before close of body
     (h/include-js  "/js/jquery.min.js")
     (h/include-js  "/js/bootstrap.min.js")]]))