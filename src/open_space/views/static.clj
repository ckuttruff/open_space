(ns open-space.views.static
  (:require [hiccup.core :as hiccup])
  (:require [open-space.views [layout :as layout]]))

(defn home []
  (layout/main
   (hiccup/html
    [:h2 "Mission"]
    [:b [:p "To build the best possible darkroom, workspace, and café for the Portland community"]]
    [:p "Whether you're coming in to do darkroom work, grab a delicious cup of coffee or snack with a friend, play some speed chess, read a book, check out a talk from a community member about a subject of interest... Open Space PDX is a space for you - built by Portlanders dedicated to the betterment and celebration of the beautiful culture here in the pacific NW."]
    [:hr]
    [:h2 ""])))

(defn darkroom []
  (layout/main (hiccup/html [:p "TODO"])))
(defn workspace []
  (layout/main (hiccup/html [:p "TODO"])))
(defn cafe []
  (layout/main (hiccup/html [:p "TODO"])))
(defn about []
  (layout/main (hiccup/html [:p "TODO"])))
(defn contact []
  (layout/main (hiccup/html [:p "TODO"])))